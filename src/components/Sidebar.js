import logo from '../assets/images/logo.svg';
import Icon from '@mdi/react'
import { mdiAccountOutline, mdiBriefcaseOutline, mdiGlasses, mdiPostOutline } from '@mdi/js'
import ReactTooltip from 'react-tooltip';

const Sidebar = ({changeMenu, menuName, openMenu}) => {
    return ( 
        <aside className="col-1">
          <nav className="navbar my-4">
              <div className="nav-group">
                  <ul className="navbar-nav">
                      <li>
                          <a href="/">
                              <img src={logo} width="40px" alt="logo RAM"/>
                          </a>
                      </li>
                      <li className={menuName === '' && (openMenu === 'close' || openMenu === 'open') ? 'active' :''}
                        data-tip="About Me"
                      > 
                          <Icon path={mdiAccountOutline} size={1} className="iconSize" onClick={() => changeMenu('')}/>
                          <ReactTooltip place="left" effect="solid" />
                      </li>
                      <li className={menuName === 'portfolio' && openMenu === 'open' ? 'active' :''}> 
                          <Icon path={mdiBriefcaseOutline} size={1} className="iconSize" onClick={() => changeMenu('portfolio')} data-tip="My Portfolio"/>
                          <ReactTooltip place="left" effect="solid" />
                      </li>
                      <li className={menuName === 'experiences' && openMenu === 'open' ? 'active' :''}> 
                          <Icon path={mdiGlasses} size={1} className="iconSize" onClick={() => changeMenu('experiences')} data-tip="My Experiences"/>
                          <ReactTooltip place="left" effect="solid" />
                      </li>
                      <li className={menuName === 'blogs' && openMenu === 'open' ? 'active' :''}> 
                          <Icon path={mdiPostOutline} size={1} className="iconSize" onClick={() => changeMenu('blogs')} data-tip="Code Diary"/>
                          <ReactTooltip place="left" effect="solid" />
                      </li>
                  </ul>
              </div>
          </nav>
      </aside>
     );
}
 
export default Sidebar;