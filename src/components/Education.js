const Education = ({eduData}) => {
  return ( 
    <section id="education" className="mb-4">
    {
      eduData.map(data => (
        <article key={data.id}>
          <p className="eduPeriod mb-0">{data.period}</p>
          <p className="eduTitle mb-0">{data.edu}</p>
          <p>{data.desc}</p>
          <p>{data.GPA !== '' ? 'GPA: ' + data.GPA : ''}</p>
        </article>
      ))
    }
    </section>
  );
}
 
export default Education;