import React, { useState } from 'react';
import Sidebar from './Sidebar'
import Me from './Me'
import Content from './Content'
function Home (){
  // render() {
    const [openMenu, setOpenMenu] = useState('close')
    const [menuName, setMenuName] = useState('')

    const changeMenu = (name) => {
      setMenuName(name)
      setOpenMenu('open')
    }  
    return(
      <section className="container main-content">
        <div className="row">
          {/* <Sidebar setOpenMenu={setOpenMenu} setMenuName = {setMenuName} openMenu={openMenu} menuName={menuName}/> */}
          <Sidebar changeMenu={changeMenu} openMenu={openMenu} menuName={menuName}/>
          <Me/>
          <Content menuName={menuName}/>
        </div>
      </section>
    )
  // }
}
export default Home;