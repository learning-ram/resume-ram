import React, { useState, useEffect } from 'react';
import Icon from '@mdi/react'
import Education from './Education'
import { mdiSchoolOutline, mdiChartPie  } from '@mdi/js'
import Portfolio from './pages/Portfolio'
import Skills from './pages/Skills'
import Experiences from './pages/Experiences'
import Codediary from './pages/CodeDiary'
import edu from './data/edu.json'
import portfol from './data/portfolio.json'
import experiences from './data/experiences.json'
import skills from './data/skills.json'
const Content = ({menuName}) => {
    const [eduData, setEdu] = useState(null);
    const [portfolData, setPortfolData] = useState(null)
    const [expData, setExp] = useState(null)
    const [skillsData, setSkillsData] = useState(null)
    useEffect(() => {
        // Fungsi nya sama seperti watch di Vue. Fungsi array dibawah adalah list const yang akan di watch. 
        // jika array nya kosong fungsi ini akan berjalan satu kali ketika render di awal (seperti created di VUE)
        // jika tidak di define, akan watch ke semua variable
        setEdu(edu.data)
        setPortfolData(portfol.data)
        setExp(experiences.data)
        setSkillsData(skills.data)
    }, [])
    switch(menuName){
        case 'portfolio':
            return (
                <section id="content" className="col-7 p-5">
                    <h1>
                        <span>portfolio</span>
                    </h1>
                    <p>It's all about what I have been working on to this day</p>
                    <Portfolio portfolData={portfolData}/>
                </section>
            )
            // break;
        case 'experiences':
            return (
                <section id="content" className="col-7 p-5">
                    <h1>
                        <span>experiences</span>
                    </h1>
                    {expData && <Experiences expData={expData}/>}
                </section>
            )
        case 'blogs':
            return (
                <Codediary/>
            )
        default:
            return (
                <section id="content" className="col-7 p-5">
                    <h1>
                        <span>about</span> me
                    </h1>
                    <p className="age">{new Date().getFullYear() - 1996} years</p>
                    <p>
                        Lorem Ipsum adalah contoh teks atau dummy dalam industri percetakan dan penataan huruf atau typesetting. Lorem Ipsum telah menjadi standar contoh teks sejak tahun 1500an, saat seorang tukang cetak yang tidak dikenal mengambil sebuah kumpulan teks dan mengacaknya untuk menjadi sebuah buku contoh huruf. Ia tidak hanya bertahan selama 5 abad, tapi juga telah beralih ke penataan huruf elektronik, tanpa ada perubahan apapun.
                    </p>
                    <hr/>
                    <h1>
                        <Icon path={mdiChartPie } size={1} className="iconSize"/> 
                        <span> skills</span>
                    </h1>
                    {skillsData && <Skills skillsData={skillsData}/> }
                    <hr/>
                    <h1>
                        <Icon path={mdiSchoolOutline} size={1} className="iconSize"/> 
                        <span> education</span>
                    </h1>
                    {eduData && <Education eduData={eduData}/>}
                </section>
            )
    }
}
 
export default Content;