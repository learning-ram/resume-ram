import React, { Component } from 'react';
import Icon from '@mdi/react'
import { mdiGitlab, mdiLinkedin   } from '@mdi/js'

class Me extends Component {
    render () {
        return (
            <section className="bg-white wrap-avatar col-4 p-0">
                <div className="avatar" style={{backgroundImage: "url('images/me.png')"}}>
                    <div className="contact p-1">
                        <p className="name mb-0">Rama Aditya Maulana</p>
                        <p className="mb-0" id="mail">Web Developer | ramaadtym@gmail.com</p>
                        <a href="#"><Icon path={mdiGitlab} size={1} className="iconLink"/> </a>
                        <a href="#"><Icon path={mdiLinkedin } size={1} className="iconLink"/> </a>
                    </div>
                </div>
            </section>
        )
    }
}

export default Me