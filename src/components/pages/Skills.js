import { ProgressBar } from "react-bootstrap";
const Skills = ({skillsData}) => {
  // <ProgressBar animated now={60} label="Photoshop" />
  return ( 
    <section id="skills">
      {
        skillsData.map(data => (
          <article key={data.id}>
            <p>{data.name}</p>
            <ProgressBar now={data.progress} label={`${data.progress}%`} className="mb-3"/> 
          </article>
        ))
      }
    </section>
   );
}
 
export default Skills;