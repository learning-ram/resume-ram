import { useState, useEffect } from 'react';
import Icon from '@mdi/react'
import { mdiBriefcaseOutline, mdiClockOutline } from '@mdi/js';
const Experiences = ({expData}) => {

  return ( 
    <section id="experiences" className="mt-4">
      {/* <Icon path={mdiBriefcaseOutline} size={1} className="iconSize"/> */}
      {
        expData.map(data => (
          <article className="d-flex align-items-center mb-3" key={data.id}>
          <figure>
            <img src={data.img} alt="logo" className="img-fluid"/>
          </figure>
          <div className="expContent mx-3">
            <p className="font-weight-bold expTitle mb-0">{data.company}</p>
            <p className="workAs mb-2">{data.title} &nbsp;
              <Icon path={mdiBriefcaseOutline} size='5px' className="iconSize"/> {data.worktype} &nbsp; 
              <Icon path={mdiClockOutline} size='5px' className="iconSize"/> {data.period}
            </p>
            <p className="desc">{data.description}</p>
          </div>
        </article>
        ))
      }
    </section>
   );
}
 
export default Experiences;