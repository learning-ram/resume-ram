import React, { Component } from 'react';
class CodeDiary extends Component { 
  
    render () {
        return (
            <section id="content" className="col-7 p-5">
                    <h1>
                        <span>#codediary</span>
                    </h1>
                    <p>
                        it's all about my code diary. Article about tutorials, information, or anything related to the front end web development.
                    </p>
                </section>
        )
    }
}
export default CodeDiary;
