import { useState } from 'react';
import { Modal, Button} from "react-bootstrap";
const Portfolio = ({portfolData}) => {
  const [show, setShow] = useState(false);
  // Define data modal
  const [dataModal, setdataModal] = useState(null);
  const handleClose = () => setShow(false);
  const handleShow = (id) => {
    const data = portfolData.filter(blog => blog.id === id);
    setdataModal(data)
    setShow(true)
  };
  return ( 
    <section id="portfolio" className="mb-4">
      {
        portfolData.map(data => (
          <article key={data.id}>
            <figure>
                <img src={data.img} className="img-fluid"/>
                <section className="detail" onClick={() => handleShow(data.id)}>
                    <div className="content">
                        <p className="font-weight-bold title">{data.name}</p>
                        <p className="createwith">{data.createWith}</p>
                    </div>
                </section>
            </figure>
          </article>
        ))
      }
      {
        dataModal && 
        dataModal.map(data => (
            <Modal show={show} onHide={handleClose} key={data.id} size="lg">
              <Modal.Header closeButton>
                <Modal.Title>{ data.name }</Modal.Title>
              </Modal.Header>
              <Modal.Body>
                <figure>
                  <img src={data.img} className="img-fluid"/>
                </figure>
                <p className="font-weight-bold">Created with: {data.createWith} - {data.link !== '' ? (<a href={data.link} target="_blank">Visit {data.name}</a>):''}</p>
                <p>{data.desc}</p>
                {
                  data.features.length > 0 ? (<p>The features that this application offers are</p>) : data.workingOn.length > 0 ? (<p>What i have finished </p>) : ''
                }
                <ol>
                  {
                    data.features.length > 0 ? data.features.map(feature => (
                      <li key={feature.id}>{feature.name}</li>
                    )) : 
                    data.workingOn.map(work => (
                      <li key={work.id}>{work.name}</li>
                    ))
                  }
                </ol>
                <hr/>
                <section id="gallery">
                  <h4>Screenshot</h4>
                  {/* <div className="d-flex"> */}
                    {data.gallery.map(data => (
                      <figure className="text-center"  key={data.id}>
                        <img src={data.img} style={{width: 80 + '%', margin:'0 auto'}} onClick={()=> window.open(data.img, '_blank')} title="click to zoom"/>
                      </figure>
                    ))}
                  {/* </div> */}
                </section>
              </Modal.Body>
              <Modal.Footer>
                <Button variant="danger" onClick={handleClose}>
                  Close
                </Button>
              </Modal.Footer>
            </Modal>
          ))
      }
    </section>
   );
}
 
export default Portfolio;